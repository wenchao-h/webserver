#! usr/bin/perl
use 5.010;
use Socket;
use Carp;
use FileHandle;
use HTML::Template;

###################### Content-type hash #########################
%Content_type = (
    ico => 'x-icon',
    jpg => 'image/jpeg',
    jpeg => 'image/jpeg',
    txt => 'text/plain',
    html => 'text/html',
);
###################### OK response headers ########################
%OK_headers = (
   start_line => 'HTTP/1.0 200 OK',
   headers => {
       Connection => 'close',
    },
);
##################### 404 Not Found headers #######################
%Not_Found_404 = (
    start_line => 'HTTP/1.0 404 Not Found',
    headers => {
        Connection => 'close',
        'Content-type' => 'text/html',
    },
);


#######################  ducument root path  ########################
$Document_root = './static';

$port = (@ARGV ? $ARGV[0]: 8080);

$proto = getprotobyname('tcp');
socket(S, PF_INET, SOCK_STREAM, $proto) || die;
setsockopt( S, SOL_SOCKET, SO_REUSEADDR, pack("l",1))|| die;
bind (S, sockaddr_in($port, INADDR_ANY)) || die;
listen(S, SOMAXCONN) || die;

printf("<< type o server accepting on port %d>>>\n\n",$port);

while(1) {
    my %request = ();
    $cport_caddr = accept(C,S);
    ($cport, $caddr) = sockaddr_in($cport_caddr);
    $client_addr = inet_ntoa($caddr);
    say "client port is $cport, client address is $client_addr";
    C ->autoflush(1);
    
    $cname = gethostbyaddr($caddr,AF_INET);
    printf("<<<Request From '%s'>>>\n",$cname);
   
    #############  parse request ################### 
    local $/ = Socket::CRLF;
    while(<C>) {
        chomp;
        if (/\s*(\w+)\s*([^\s]+)\s*HTTP\/(\d.\d)/) {
            $request{method} = uc $1;
            $request{url} = $2;
            $request{http_version} = $3;
        } elsif (/:/) {
            (my $type, my $var) = split /:/,$_, 2;
            $type =~ s/^\s+//;
            foreach ($type, $var) {
                  s/^\s+//;
                  s/\s+$//;
            }
            $request{lc $type} = $var;
        } elsif (/^$/) {
            read(C, $request{content}, $request{'content-length'}) 
                if defined $request{'content-length'};
            last;
        }
    }
    while ( (my $key, my $value) =  each %request) {
        say "$key : $value"; 
    }
######################## handle request #############################
    handler(%request);    
    close(C);
}


##################### sort out method ###############################
sub handler {
    my %request = @_;
    if ($request{method} eq 'GET') {
        print "method is $request{method} \n";
        return handle_get(%request);
    } elsif ($request{method} eq 'POST') {
        print "method is $request{method}\n";
        return handle_post(%request);
    }
}

#################### handle get request ############################
sub handle_get {
    print "in handle_get\n";
    my %request = @_;
    my $url = $request{url};
    if ($url =~ /.*\?.*/) { 
        #$url =~ s/.*\?//;
        #my @request_fields = split /&/,$url;
        ############add function to deal with request #############
        #$url =~ /\/([\w|\.]+)\?([\w|=]+)/; 
        #my $script = $1;
        #my $query_string = $2;
        print "in cgi\n"; 
        $url =~ /\/([\w|\.]+)\?([\w|=]+)/;
        my $script = $1;
        print "script name is $script\n";
        my $query_string = $2;
        print "string is $query_string\n";
        $ENV{QUERY_STRING }=$query_string;
        $ENV{SCRIPT} = $script;
        my %headers = %OK_headers;
        trans_headers_cgi(%headers);
        my $pid = fork();
        if ($pid==0) {
            open STDOUT,'>&',C;
            exec('./'.$script);
            exit 0; 
        } 
        waitpid($pid,0);
        return; 
    }

    
    if (-d $Document_root.$url) {
        print "dir exist\n";
        my $file_path = $Document_root.$url;
        my %headers = %OK_headers;
        $headers{headers}->{'Content-type'}='text/html';
        trans_index($file_path,%headers);
        return;
    }
    if (-e $Document_root.$url) {
        print "file exist\n";
        my %headers = %OK_headers;
        my $file_path = $Document_root.$url;
        my $post_fix = $url =~ /.*\.(\w+)$/;
        print "post_fix is $post_fix\n";
        my $content_type = $Content_type{$post_fix};
        $headers{headers}->{'Content-type'} = $content_type;
        trans_response(%headers,$file_path);
        return ;
    }
    my %pattern = (
        '/search' => \&search_func,
    );
    if (exists $pattern{$url}) {
        print "pattern match\n";
        my $handler = $pattern{$url};
        $handler -> (%request);
        return;
    }
    print " now 404\n";
    my $file_path = 'Not_Found_404.html';
    trans_response(%Not_Found_404,$file_path);
    return;
}
####################### trans response ##################################
sub trans_response {
    print "in trans_response\n";
    my $file_path = pop @_;
    my %headers = @_;
    trans_headers(%headers);
    if (open ( my $fh, '<',$file_path)) {
        my $buffer;
        while (read ($fh,$buffer,4096)) {
            print C $buffer;
        }
        close($fh);
    }
}

##################### trans index ####################################
sub trans_index {
    print "in trans_index\n";
    print "@_" . "\n";
    my ($file_path,%headers) = @_;
    print "file path is $file_path\n";
    my $url = $file_path;
    $url =~ s/.\/static//;
    $url = $url .'/' unless $url =~ /\/$/;
    print "url is $url\n";
    while (my ($key, $value) = each %headers) {
        print "$key \n";
    }
    opendir my $dir, $file_path or die "cannot open directory:$! \n";
    my @files = readdir $dir;
    close $dir;
    print "@files\n";
    my @lists;
    for (@files) {
        
        my $dict = {
            href => $url.$_,
            file => $_,
        };
        push @lists,$dict;
    }
    print "i am here\n"; 
    my $template = HTML::Template -> new(filename => './template/list_file.tmpl');
    $template -> param (
        FILE_LIST => \@lists
    );
    trans_headers(%headers); 
    print C $template ->output();
}    

###############################trans headers ###################
sub trans_headers {
    my %headers = @_;
    print C $headers{start_line}, Socket::CRLF;
    while (my ($type, $content) = each %{$headers{headers}}) {
        print C "$type:$content",Socket::CRLF;
    }
    print C Socket::CRLF;
}

############################ trans headers cgi ##################
sub trans_headers_cgi {
    my %headers = @_;
    print C $headers{start_line}, Socket::CRLF;
    while (my ($type, $content) = each %{$headers{headers}}) {
        print C "$type:$content",Socket::CRLF;
    }
}

######################### search function ###########################
sub search_func {
    print "in search_func\n";
    my %request = @_;
    my %headers = %OK_headers;
    my $file_path = 'search.html';
    $headers{headers}->{'Content-type'}='text/html';
    trans_response(%headers,$file_path);
}



